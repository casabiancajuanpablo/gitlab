# README #

This is the GitLab Marketing Web Developer/Designer.
The project is crated from a simple frontend template created by JP Casabianca.

### What is this repository for? ###

* Build frontend projects.

### How do I get set up? ###

* It's really simple... 
* Run "npm install" to install all node modules.
* To watch for file changes run "gulp watchFiles".
* Then, run "gulp" to build the dist folder.
* And VOILA! 
