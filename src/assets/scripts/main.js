// MOBILE MEN Dropdown
$('#icon-menu-mobile').click(function(e){
	e.stopPropagation();
	if(!$('#menu-dropdown').hasClass('open')) {		
		$('#menu-dropdown').addClass('open');
		$(this).removeClass("icon-menu");
		$(this).addClass("icon-x");
	} else {
		$('#menu-dropdown').removeClass('open');
		$(this).addClass("icon-menu");
		$(this).removeClass("icon-x");
	}
	$('#menu-dropdown').slideToggle('slow');
});

// Search Header Functionality
$('#iconsearch').click(function(e){
	e.stopPropagation();
	$('#search').addClass('open');
	$('#searchinput').focus()
	$('#search').fadeToggle();
});

// Close Search in Header
$('#closesearch').click(function(e){
	e.stopPropagation();
	$('#search').removeClass('open');
	$('#search').fadeToggle();
});

// Prevent containers to close elements when open
$(document).on('click', '#menu-dropdown', 'header', function(e){
	e.stopPropagation();
});

// Escape closes mobile menu or search if open.
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
        if($('#menu-dropdown').hasClass('open')) {		
			$('#menu-dropdown').removeClass('open');
			$('#icon-menu-mobile').addClass("icon-menu");
			$('#icon-menu-mobile').removeClass("icon-x");
			$('#menu-dropdown').slideToggle('slow');
		}
		if($('#search').hasClass('open')) {
			$('#search').removeClass('open');
			$('#search').fadeToggle();
		}
    }
});

// User can click anywhere on the site to close mobile menu
$(document).on('click', 'html', function(e) {
	if($('#menu-dropdown').hasClass('open')) {		
		$('#menu-dropdown').removeClass('open');
		$('#icon-menu-mobile').addClass("icon-menu");
		$('#icon-menu-mobile').removeClass("icon-x");
		$('#menu-dropdown').slideToggle('slow');
	}
});

// Scrolls to form when person clicks CTA
$(".scroll-to-form").click(function(e) {
	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#section1").offset().top
    }, 1200, function() {
    	$('.nameinputfocus').focus();
  	});
});

// Owl Carousel for Testimonies, Section 5
$(document).ready(function(){
  	$('.owl-carousel').owlCarousel({
	    animateOut: 'slideOutDown',
	    animateIn: 'flipInX',
	    items:1,
	    smartSpeed:450,
	    dots:true,
	    autolay: true,
	    autoplayTimeout: 1000
	});
});

// Prevent default action for anchors and send link in href
$(".link").click(function(e) {
	e.preventDefault();
	link = $(this).attr("href");
	window.location.href = "https://about.gitlab.com/" + link;
});

// Form submisission. If succesfull, it toggles between main landing page and thank you page.
$(document).ready(function() {
    $(".form").submit(function() { 
        $("#main-landing").fadeToggle();
        $("#thanks-landing").fadeToggle();
    	$('#message_submission').html("Get started with GitLab today!");
    	$('#button_submission').addClass("link")
    	$('#button_submission').removeClass("scroll-to-form")
    	$('#button_submission').html("START TODAY")
    });
});





